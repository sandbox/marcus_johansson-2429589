<?php

/**
 * @file
 * Page for controlling the user administration area.
 */

/**
 * Administration area for Combined Images.
 */
function combinedimages_admin() {
  
  $form['combinedimages_type'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('combinedimages_type', '0'),
    '#title' => t('Type of serving'),
    '#description' => t('How should the combined images be served to the end user'),
    '#options' => array(
        '0' => t('Disabled (normal images)'), 
        '1' => t('Inline (all in html)'), 
        '2' => t('CSS file and inline (in external CSS file)')
    ),
  );
  
  $form['combinedimages_how'] = array(
    '#type' => 'checkboxes',
    '#default_value' => variable_get('combinedimages_how', array('1' => '1', '2' => '0')),
    '#title' => t('Where'),
    '#description' => t('Where should it be changed for the end user'),
    '#options' => array(
        '1' => t('All theme_image() and theme_image_style()'), 
        '2' => t('Node view')
    ),
    '#states' => array(
      'invisible' => array(
        'input[name="combinedimages_type"]' => array('value' => '0'),
      ),
    ),
  );
  
   $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Only show on specific paths'),
  );

  $form['visibility']['combinedimages_show'] = array(
    '#type' => 'radios',
    '#default_value' => variable_get('combinedimages_show', '0'),
    '#options' => array(
      '0' => t('All pages except those listed'),
      '1' => t('Only the listed pages'),
      '2' => t('Pages on which this PHP code returns TRUE (experts only, requires PHP module)'),
    ),
  );
  
  if (!module_exists('php') || !user_access('use PHP for settings')) {
    $form['visibility']['combinedimages_show']['2'] = array('#disabled' => TRUE);
  }  

  $form['visibility']['combinedimages_paths'] = array(
    '#type' => 'textarea',
    '#default_value' => variable_get('combinedimages_paths', 'admin/*'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page. If the PHP option is chosen, enter PHP code between <?php ?>. Note that executing incorrect PHP code can break your Drupal site."),
    '#required' => FALSE,
  );

  $form['combinedimages_user_roles'] = array(
    '#type' => 'checkboxes',
    '#multiple' => TRUE,
    '#title' => t('Only enable for certain user roles'),
    '#default_value' => variable_get('combinedimages_user_roles', array('1')),
    '#description' => t('Only let certain roles be served by combined images. In 99% of cases you should only do this for anonymous users and when you can cache.'),
    '#options' => user_roles(),
    '#states' => array(
      'invisible' => array(
        'input[name="combinedimages_type"]' => array('value' => '0'),
      ),
    ),
  );
  
  $options[0] = t('Unlimited');
  for($i = 1; $i < 11; $i++) {
    $options[$i] = t('!amount file(s)', array('!amount' => $i));
  }
  
  $form['combinedimages_split_files'] = array(
    '#type' => 'select',
    '#title' => t('Split CSS after'),
    '#default_value' => variable_get('combinedimages_split_files', '0'),
    '#description' => t('Split into a new CSS after x amount of images'),
    '#options' => $options,
    '#states' => array(
      'visible' => array(
        'input[name="combinedimages_type"]' => array('value' => '2'),
      ),
    ),
  );
  
  $options = array(t('Unlimited'));
  for($i = 1; $i < 11; $i++) {
    $options[$i*100] = t('!amount kb(s)', array('!amount' => $i*100));
  }
  
  $form['combinedimages_split_size'] = array(
    '#type' => 'select',
    '#title' => t('or Split CSS after'),
    '#default_value' => variable_get('combinedimages_split_size', '0'),
    '#description' => t('or split into a new CSS after x amount of kbytes'),
    '#options' => $options,
    '#states' => array(
      'visible' => array(
        'input[name="combinedimages_type"]' => array('value' => '2'),
      ),
    ),
  );
  
  $options = array(t('None'));
  for($i = 1; $i < 11; $i++) {
    $options[$i*10] = t('!amount kb(s)', array('!amount' => $i*10));
  }
  
  $form['combinedimages_inline_size'] = array(
    '#type' => 'select',
    '#title' => t('and inline files under'),
    '#default_value' => variable_get('combinedimages_inline_size', '0'),
    '#description' => t('and let files under X kb still be inlined'),
    '#options' => $options,
    '#states' => array(
      'visible' => array(
        'input[name="combinedimages_type"]' => array('value' => '2'),
      ),
    ),
  );  
  
  return system_settings_form($form);
}