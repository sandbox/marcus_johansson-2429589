INTRODUCTION
---------------
The Combined Images module takes all images that are about to be loaded from
a node or from theme_image() and make it into a base64 encoded string instead.

This can be presented like an image to the end user, but it does not require
you to load the image as an separate request to the server.

REQUIREMENTS
--------------
No requirements except curl

INSTALLATION
--------------
1. Install as you would normally install a contributed module

2. Visit admin/config/media/combinedimages and setup the settings as
you want.

3. Make sure you empty all caches after doing changes



MAINTAINERS
----------------
Current maintainers:
 * Marcus Johansson (https://www.drupal.org/u/marcus_johansson)
 
This project was sponsored by:
 * Marcus Johansson Drupal Consultancy
   Specialized in Drupal module development. I offer development
   of modules, themes and hosting in Berlin, Sweden and Spain as well as
   on distance. me@marcusmailbox.com http://www.drupaldare.com